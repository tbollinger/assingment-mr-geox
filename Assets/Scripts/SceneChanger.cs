using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// managed sub scene navigation
/// </summary>
public class SceneChanger : MonoBehaviour
{
    [SerializeField] private string defaultScene = default;

    private Scene _currentScene;
        
    private void Start()
    {
        LoadScene(defaultScene);
    }

    private void OnSceneLoaded(string sceneName)
    {
        _currentScene = SceneManager.GetSceneByName(sceneName);
    }
        
    public void LoadScene(string sceneName)
    {
        if (_currentScene.isLoaded)
        {
            SceneManager.UnloadSceneAsync(_currentScene);
        }
            
        var operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        operation.allowSceneActivation = true;
        operation.completed += asyncOperation => OnSceneLoaded(sceneName);
    }
}