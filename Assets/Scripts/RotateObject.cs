using UnityEngine;

/// <summary>
/// rotating the object around its local z axis
/// </summary>
public class RotateObject : MonoBehaviour
{
    [SerializeField] private float rotatingSpeed = 4f;
    [SerializeField] private ToggleButton rotateButton = default;

    private bool IsActive { get; set; }
        
    private void Start()
    {
        IsActive = true;
        rotateButton.OnToggle += ToggleRotateState;
    }

    private void ToggleRotateState(object sender, bool e)
    {
        IsActive = e;
    }

    private void Update()
    {
        if (IsActive)
        {
            transform.rotation *= Quaternion.AngleAxis(Time.deltaTime * rotatingSpeed, transform.up);
        }
    }
}