using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

/// <summary>
/// Info Text to explain the controls
/// </summary>
public class InfoText : MonoBehaviour
{
    [SerializeField] private SimpleButton infoButton = default;
    
    private readonly IReadOnlyList<KeyCode> _movementKeys = new[]
    {
        KeyCode.W, 
        KeyCode.S, 
        KeyCode.A, 
        KeyCode.D, 
        KeyCode.LeftArrow, 
        KeyCode.RightArrow, 
        KeyCode.DownArrow,
        KeyCode.UpArrow
    };

    private RectTransform _rectTransform;
    private bool _lock = false;
    private bool _visible = true;
    
    private float Height =>
        _rectTransform.sizeDelta.y;
    
    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _rectTransform.localScale = new Vector3(1, 0, 1);
        Show();
        infoButton.OnClick += (sender, _) => Show();
    }

    private void Show()
    {
        if(_lock) return;
        _lock = true;
        
        _rectTransform.transform
            .DOScaleY(1, 1f)
            .SetEase(Ease.OutCubic)
            .OnComplete(() => (_visible, _lock) = (true, false));
    }

    private void Hide()
    {
        if(_lock) return;
        _lock = true;
        _visible = false;
        _rectTransform.transform
            .DOScaleY(0, 1f)
            .SetEase(Ease.OutCubic)
            .OnComplete(() => _lock = false);
    }
    
    private void Update()
    {
        if (_visible && (Input.GetMouseButton(0) || _movementKeys.Any(Input.GetKey) || Input.mouseScrollDelta.sqrMagnitude > 0.1f))
        {
            Hide();
        }
    }
}