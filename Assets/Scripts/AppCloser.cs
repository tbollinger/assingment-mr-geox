using System;
using UnityEngine;

/// <summary>
/// Closes the app
/// </summary>
public class AppCloser : MonoBehaviour
{
    [SerializeField] private SimpleButton quitButton;

    private void Start()
    {
        quitButton.OnClick += (sender, args) => Application.Quit();
    }
}
