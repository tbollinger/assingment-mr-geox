using System;
using UnityEngine;

/// <summary>
/// Changes the skybox by changing the sub scene. 
/// </summary>
public class ChangeSkyBoxController : MonoBehaviour
{
    [SerializeField] private string[] scenes = default;
    [SerializeField] private SimpleButton changeSkyBoxButton = default;
    [SerializeField] private SceneChanger sceneChanger = default;
    
    private int _currentSceneId = 0;

    private void Start()
    {
        changeSkyBoxButton.OnClick += ChangeSkyBox;
    }

    private void ChangeSkyBox(object sender, EventArgs e)
    {
        _currentSceneId = (_currentSceneId + 1) % (scenes.Length);
        sceneChanger.LoadScene(scenes[_currentSceneId]);
    }
}
