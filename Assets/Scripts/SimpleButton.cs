using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simple button with hotkey
/// </summary>
public class SimpleButton : MonoBehaviour
{
    [SerializeField] private KeyCode alternativeButton = default;
        
    private Button _button;

    public event EventHandler OnClick;
        
    private void Start()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        OnClick?.Invoke(this, EventArgs.Empty);
    }
        
    private void Update()
    {
        if (Input.GetKeyDown(alternativeButton))
        {
            OnButtonClick();
        }
    }
}