﻿using System;
using UnityEngine;

/// <summary>
/// Works together with the Cinemachine packages to orbit around an object
/// </summary>
[RequireComponent(typeof(MouseInputController))]
public class CameraRotateScript : MonoBehaviour
{
    #region UnityInspectorVariables
    [SerializeField] private Transform target = default;
    [SerializeField] private float verticalSpeed = 4f;
    [SerializeField] private float horizontalSpeed = 4f;
    [SerializeField] private float forwardSpeed = 4f;
    [SerializeField] private float mouseDeltaSpeed = 4f;

    [SerializeField] private float maxDistance = 4f;
    [SerializeField] private float minDistance = 0.5f;
    #endregion
    
    private float _dist;
    private MouseInputController _mouseInputController;

    private void Start()
    {
        _dist = Vector3.Distance(target.position, transform.position);
        _mouseInputController = GetComponent<MouseInputController>();
    }

    private void Update()
    {
        var (horizontal, vertical) = (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        var hVec = horizontal * Time.deltaTime * verticalSpeed * transform.right;
        var vVec = vertical * Time.deltaTime * horizontalSpeed * transform.up;

        _dist -= Input.mouseScrollDelta.y * forwardSpeed;
        _dist = Mathf.Clamp(_dist,  minDistance, maxDistance);
        Rotate(hVec + vVec + MouseDelta());
    }
    
    private Vector3 MouseDelta()
    {
        var absoluteMouseDelta = _mouseInputController.DeltaMovement * -mouseDeltaSpeed;
        return absoluteMouseDelta.x * transform.right + absoluteMouseDelta.y * transform.up;
    }

    /// <summary>
    /// Make a movement step while keep the distance to the object
    /// </summary>
    /// <param name="direction">The direction vector of the movement direction</param>
    private void Rotate(Vector3 direction)
    {
        var t = transform;
        var targetPos = target.position;
        var currentVec = t.position - targetPos;
        var newVec = currentVec + direction;
        
        newVec = newVec.normalized * _dist;

        var newPos = targetPos + newVec;
        if (Vector3.Dot(-newVec.normalized,Vector3.up) > 0.95f)
        {
            return;
        }
        else if (Vector3.Dot(-newVec.normalized, Vector3.up) < -0.95f)
        {
            return;
        }
        t.position = newPos;
    }
}
