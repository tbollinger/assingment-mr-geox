using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Toggle button with hotkey
/// </summary>
public class ToggleButton : MonoBehaviour
{
    [SerializeField] private Color disableColor = default;
    [SerializeField] private Color enableColor = default;
    [SerializeField] private bool defaultState = default;
    [SerializeField] private KeyCode alternativeButton = default;
        
    private Image _image;
    private bool _state;
    private Button _button;

    public event EventHandler<bool> OnToggle;
        
    private void Start()
    {
        _state = defaultState;
        _image = GetComponent<Image>();
        _button = GetComponent<Button>();
        _image.color = GetStateColor();
        _button.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        _state = !_state;
        _image.color = GetStateColor();
        OnToggle?.Invoke(this, _state);
    }
        
    private Color GetStateColor() =>
        (_state) ? enableColor : disableColor;

    private void Update()
    {
        if (Input.GetKeyDown(alternativeButton))
        {
            OnButtonClick();
        }
    }
}