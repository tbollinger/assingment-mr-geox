using System;
using UnityEngine;

/// <summary>
/// Captures the mouse delta movement when the left mouse button is clicked
/// </summary>
public class MouseInputController : MonoBehaviour
{
    private Vector3 _lastPos;

    public Vector3 DeltaMovement { get; private set; }
        
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _lastPos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            DeltaMovement = Input.mousePosition - _lastPos;
            _lastPos = Input.mousePosition;
        }
        else
        {
            DeltaMovement = Vector3.zero;
        }
    }
}