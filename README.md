# Assignment Mr. Geox

A simple 3D viewer for a Geox shoe. It is possible to rotate the shoe around the y-axis (left-handed coordinate system, x left, y up, z forward), rotate the camera around the object with WSAD or with the mouse when holding the left mouse button and zooming in with the mouse wheel. It is possible to change the skybox.

[Download Windows Version Here](https://gitlab.com/tbollinger/assingment-mr-geox/-/raw/master/bin/Windows.zip?inline=false)

## C# files
All written C# files are in the Assets/Scripts directory.

## Tools used

### Unity
**version**: 2020.1.6f1

Unity game engine.

### Cinemachine ([link](https://docs.unity3d.com/Packages/com.unity.cinemachine@2.6/manual/index.html))

**version**: 2.6.2 

A handy unity package to manage the camera movement, used for a simple look at the object. Combined with the written script `CameraRotateScript.cs`.

### DOTween ([link](http://dotween.demigiant.com/))
**version**: 1.2.420

Animation engine used for animating UI Elements.

### unity3d ci from Gabriel Le Breton ([link](https://gitlab.com/gableroux/unity3d-gitlab-ci-example))
Scripts for building the unity project.

## Assets used

### HDRI Skyboxes
From [hdrihaven.com](https://hdrihaven.com)

* [aft_lounge](https://hdrihaven.com/hdri/?h=aft_lounge)
* [tears_of_steel_bridge](https://hdrihaven.com/hdri/?h=tears_of_steel_bridge)


### Font Awesome Icons
From [https://fontawesome.com/](https://fontawesome.com/)